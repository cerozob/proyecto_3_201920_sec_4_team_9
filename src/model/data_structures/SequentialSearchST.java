package model.data_structures;

import java.util.Iterator;

public class SequentialSearchST <K extends Comparable<K>,V> implements IHashTable<K,V>
{
	private Node first;
	private int n;

	private class Node implements Iterator<Node>
	{ 
		K key;
		V val;
		Node next;
		public Node(K pKey, V pVal, Node pNext)
		{
			key = pKey;
			val = pVal;
			next = pNext;
		}
		public boolean hasNext()
		{	return next != null;	}
		public Node next()
		{	return next();	}
	}

	public SequentialSearchST()
	{

	}

	public V get(K key)
	{ 
		for (Node x = first; x != null; x = x.next)
			if (key.equals(x.key))
				return x.val; 
		return null; 
	}

	public void put(K key, V val) 
	{
		for (Node x = first; x != null; x = x.next)
			if(key.equals(x.key))
			{
				x.val = val;
				return;
			}
		first = new Node(key,val,first);
		n++;
	}

	public void putInSet(K key, V val) 
	{
		for (Node x = first; x != null; x = x.next)
			if(key.equals(x.key))
			{
				x.next = new Node(key, val, x.next);
				return;
			}
		first = new Node(key,val,first);
	}


	public V delete(K key)
	{
		V rta = null;
		Node x = first;
		while (x.next != null) {
			if(key.equals(x.key)) 
			{	rta = x.val;
			break;		}	
		}
		first = delete(first, key);
		return rta;
	}

	private Node delete(Node x, K key)
	{
		if (x == null) return null;
		if (key.equals(x.key)) {
			n--;
			return x.next;
		}
		x.next = delete(x.next, key);
		return x;
	}

	public Iterator<V> getSet(K key) 
	{
		LinkedList<V> lista = new LinkedList<V>();
		Node temp = first;
		while ( temp != null) {
			if(key.equals(temp.key))
				lista.append(temp.val);	
			temp=temp.next;
		}
		return (Iterator<V>) lista.iterator();
	}

	public Iterator<V> deleteSet(K key) 
	{
		LinkedList<V> lista = new LinkedList<V>();
		Node x = first;
		while ( x != null) {
			Node siguiente = x.next;
			if(siguiente!=null && siguiente.key.equals(x.key))
			{
				lista.append(siguiente.val); //Lo mete en la linkedList
				x.next = x.next.next; //Lo borra del SequentialSearchST 
			}
			x = x.next; 
		}
		return (Iterator<V>) lista.iterator();
	}

	public Iterator<K> keys() {
		LinkedList<K> lista = new LinkedList<K>();
		Node x = first;
		while ( x != null) 
		{
			lista.append(x.key);
			x = x.next; 
		}
		return (Iterator<K>) lista.iterator();
	}


	public int size()
	{
		return n;
	}

	public boolean isEmpty()
	{
		if(n == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean contains(K key)
	{
		if(get(key) != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public int getM() {

		return 0;
	}

	@Override
	public int contador() {

		return 0;
	}
}
