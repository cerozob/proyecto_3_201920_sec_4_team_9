package model.data_structures;

import java.util.Iterator;

public class NodeForLinkedList<T> implements Iterator<NodeForLinkedList<T>>{
	
	private NodeForLinkedList<T> next;
	private int index;
	private T object;

	public NodeForLinkedList (T pObject) 
	{
		next = null;
		object = pObject;
	}

	public NodeForLinkedList<T> getNext() 
	{	return next;	}

	public void setNext ( NodeForLinkedList<T> pNext) 
	{	next = pNext;	}

	public T getObject()
	{	return object;	}

	public void setObject (T pObject) 
	{	object = pObject;	}
	
	public int getIndex()
	{	return index;	}

	public void setIndex(int pIndex)
	{	index = pIndex;	}
	
	public boolean hasNext()
	{	return next != null;	}
	
	public NodeForLinkedList<T> next()
	{	return next;	}
}
