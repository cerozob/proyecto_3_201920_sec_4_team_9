package model.data_structures;

import java.util.Iterator;

public class DepthFirstSearch{
	
	private boolean[] marked;
	private int count;
	
	public DepthFirstSearch(Graph G, int s)
	{
		marked = new boolean[G.V()];
        validateVertex(s);
        dfs(G, s);
	}
	
	
//	// depth-first search for an EdgeWeightedGraph
//    private void dfs(Graph G, int v) {
//        marked[v] = true;
//        componentes[count].add(v);
//        id[v] = count;
//        count++;
//        Iterator<WeighedEdge> iter = G.adj(v).iterator();
//        while (iter.hasNext()) {
//            int w = iter.next().other();
//            if (!marked[w]) {
//                dfs(G, w);
//            }
//        }
//    }
	
	private void dfs(Graph G, int v) {
        count++;
        marked[v] = true;
        Iterator<WeighedEdge> iter = G.adj(v).iterator();
        while(iter.hasNext()) {
            int w = iter.next().other(v);
            if (!marked[w]) {
                dfs(G, w);
            }
        }
    }


    
	public boolean marked(int w)
	{	validateVertex(w);	return marked[w]; 	}

	public int count()
	{ 		return count;		}
	
	

	private void validateVertex(int v) {
        int V = marked.length;
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }
}
