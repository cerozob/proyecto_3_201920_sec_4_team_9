package model.data_structures;

import java.util.Iterator;

public class Graph<V> implements IGrafo<V> {
	
	private IHashTable<Integer,V> vertices;
	private IHashTable<String, V> ubicaciones;
    private final int V;
    private int E;
    private Bag<WeighedEdge>[] adj;
    private boolean[] marked;
    private Integer[] cc; 
    private DepthFirstSearch dfs;
    
    /**
     * Initializes an empty edge-weighted graph with {@code V} vertices and 0 edges.
     *
     * @param  V the number of vertices
     * @throws IllegalArgumentException if {@code V < 0}
     */
    public Graph(int V) {
        if (V < 0) throw new IllegalArgumentException("Number of vertices must be nonnegative");
        this.V = V;
        this.E = 0;
        vertices = new SeparateChainingHashST<Integer, V>(V);
        ubicaciones = new SeparateChainingHashST<String, V>(V);
        adj = (Bag<WeighedEdge>[]) new Bag[V];
        for (int v = 0; v < V; v++) {
            adj[v] = new Bag<WeighedEdge>();
        }
        marked = new boolean[V];
        for (int i = 0; i < marked.length; i++) //Inicializa el arreglo de vértices marcados en false
        {	 marked[i] = false;		}
        cc = new Integer[V];
    }
    
    /**
     * Returns the number of vertices in this edge-weighted graph.
     *
     * @return the number of vertices in this edge-weighted graph
     */
    public int V() {
        return V;
    }

    /**
     * Returns the number of edges in this edge-weighted graph.
     *
     * @return the number of edges in this edge-weighted graph
     */
    public int E() {
        return E;
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }

    /**
     * Adds the undirected edge {@code e} to this edge-weighted graph.
     *
     * @param  e the edge
     * @throws IllegalArgumentException unless both endpoints are between {@code 0} and {@code V-1}
     */
    public void addEdge(WeighedEdge e) {
        int v = e.either();
        int w = e.other(v);
        validateVertex(v);
        validateVertex(w);
        adj[v].add(e);
        adj[w].add(e);
        E++;
    }

    /**
     * Returns the edges incident on vertex {@code v}.
     *
     * @param  v the vertex
     * @return the edges incident on vertex {@code v} as an Iterable
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public Iterable<WeighedEdge> adj(int v) {
        validateVertex(v);
        return adj[v];
    }

    /**
     * Returns the degree of vertex {@code v}.
     *
     * @param  v the vertex
     * @return the degree of vertex {@code v}               
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public int degree(int v) {
        validateVertex(v);
        return adj[v].size();
    }

    /**
     * Returns all edges in this edge-weighted graph.
     * To iterate over the edges in this edge-weighted graph, use foreach notation:
     * {@code for (Edge e : G.edges())}.
     *
     * @return all edges in this edge-weighted graph, as an iterable
     */
    public Iterable<WeighedEdge> edges() {
        Bag<WeighedEdge> list = new Bag<WeighedEdge>();
        for (int v = 0; v < V; v++) {
            int selfLoops = 0;
            for (WeighedEdge e : adj(v)) {
                if (e.other(v) > v) {
                    list.add(e);
                }
                // add only one copy of each self loop (self loops will be consecutive)
                else if (e.other(v) == v) {
                    if (selfLoops % 2 == 0) list.add(e);
                    selfLoops++;
                }
            }
        }
        return list;
    }

	@Override
	public void addEdge(int idVertexIni, int idVertexFin, double cost) {
		WeighedEdge e = new WeighedEdge(idVertexIni, idVertexFin, cost); 
		addEdge(e);
	}

	@Override
	public V getInfoVertex(int idVertex) {
		return vertices.get(idVertex);
	}

	@Override
	public void setInfoVertex(int idVertex, V infoVertex) {
		if (vertices.contains(idVertex)) 
		{
			vertices.delete(idVertex);
			vertices.put(idVertex, infoVertex);
		}
		else 
			vertices.put(idVertex, infoVertex);
	}

	@Override
	public double getCostArc(int idVertexIni, int idVertexFin)
	{
		return getCostArcHaver( idVertexIni,  idVertexFin);
	}
	
	public double getCostArcHaver(int idVertexIni, int idVertexFin) {
		Iterator<WeighedEdge> iter = edges().iterator();
		while(iter.hasNext())
		{
			WeighedEdge e = iter.next();
			int ese = e.either();
			int elotro = e.other(ese);
			if(ese == idVertexIni && elotro == idVertexFin || ese == idVertexFin && elotro == idVertexIni)
			{
				return e.haver();
			}
		}
		return 0;
	}
	
	

	@Override
	public void setCostArc(int idVertexIni, int idVertexFin, double cost) {
		setCostArcHaver(idVertexIni,  idVertexFin,  cost);
	}
	
	public void setCostArcHaver(int idVertexIni, int idVertexFin, double cost) {
		Iterator<WeighedEdge> iter = edges().iterator();
		int edges = 0;
		while(iter.hasNext()&& edges != 2)
		{
			WeighedEdge e = iter.next();
			int ese = e.either();
			int elotro = e.other(ese);
			if(ese == idVertexIni && elotro == idVertexFin || ese == idVertexFin && elotro == idVertexIni)
			{
				e.setHaver(cost);
				edges++;
			}
			
		}
	}

	@Override
	public void addVertex(String key, int idVertex,V infoVertex)
	{
		if(vertices.get(idVertex) == null)
		{
			vertices.put(idVertex, infoVertex);
			ubicaciones.put(key, infoVertex);
		}
	}
	
	@Override
	public void uncheck() {
		for (int i = 0; i < marked.length; i++) {
			marked[i] = false;
		}
	}

	@Override
	public void dfs(int s) {
		 dfs = new DepthFirstSearch(this, s);
	}
	
	public int cc()
	{
		CC cc = new CC(this);
		return cc.count();
	}

	@Override
	public Iterable<Integer> getCC (int idVertex) 
	{
		CC cc = new CC(this);
		return cc.getCC(idVertex);	
	}
	
	public Iterable<V> getVertices()
	{
		Bag<V> bag = new Bag<V>();
		Iterator<Integer> iter = vertices.keys();
		while(iter.hasNext())
		{
			V v = vertices.get(iter.next());
			bag.add(v);
		}
		return bag;
	}
	
	public int getCantidadVertices()
	{
		return vertices.size();
	}
	public int getUbicaciones()
	{
		return ubicaciones.size();
	}
	
	public V getVertex(int lati, int longi)
	{
		String key = longi + "-" + lati; 
		return ubicaciones.get(key);
	}
	
	public V getVertex(int id)
	{
		return vertices.get(id);
	}
}


//Copyright � 2000�2019, Robert Sedgewick and Kevin Wayne.
//Last updated: Sat Nov 16 07:05:36 EST 2019.