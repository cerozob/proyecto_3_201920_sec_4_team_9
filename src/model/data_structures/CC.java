package model.data_structures;

import java.util.Iterator;

import model.logic.Vertice;

public class CC {
    private boolean[] marked;   // marked[v] = has vertex v been marked?
    private int[] id;           // id[v] = id of connected component containing v
    private int[] size;         // size[id] = number of vertices in given component
    private int count;          // number of connected components
    
    private LinkedList<Vertice>[] componentes;
    private LinkedList<WeighedEdge>[] relaciones;

    /**
     * Computes the connected components of the edge-weighted graph {@code G}.
     *
     * @param G the edge-weighted graph
     */
    @SuppressWarnings("unchecked")
	public CC(Graph G) {
    	int tam = G.V();
        marked = new boolean[tam];
        id = new int[tam];
        size = new int[tam];
        
        componentes = (LinkedList<Vertice>[]) new LinkedList[tam];
        for (int v = 0; v < tam; v++) 
            componentes[v] = new LinkedList<Vertice>();
        relaciones = (LinkedList<WeighedEdge>[]) new LinkedList[tam];
        for (int v = 0; v < tam; v++) 
            relaciones[v] = new LinkedList<WeighedEdge>();
        
        for (int v = 0; v < G.V(); v++) {
            if (!marked[v]) {
                dfs(G, v);
                count++;
            }
        }
    }

    // depth-first search for an EdgeWeightedGraph
    private void dfs(Graph G, int v) {
        marked[v] = true;
        id[v] = count;
        size[count]++;
        componentes[count].append((Vertice)G.getVertex(v));
        Iterator<WeighedEdge> iter = G.adj(v).iterator();
        while(iter.hasNext()) {
        	WeighedEdge e = iter.next();
        	relaciones[count].append(e);
            int w = e.other(v);
            if (!marked[w]) {
                dfs(G, w);
            }
        }
    }


    /**
     * Returns the component id of the connected component containing vertex {@code v}.
     *
     * @param  v the vertex
     * @return the component id of the connected component containing vertex {@code v}
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public int id(int v) {
        validateVertex(v);
        return id[v];
    }

    /**
     * Returns the number of vertices in the connected component containing vertex {@code v}.
     *
     * @param  v the vertex
     * @return the number of vertices in the connected component containing vertex {@code v}
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public int size(int v) {
        validateVertex(v);
        return size[id[v]];
    }

    /**
     * Returns the number of connected components in the graph {@code G}.
     *
     * @return the number of connected components in the graph {@code G}
     */
    public int count() {
        return count;
    }
    
    /**
     * devuelve un iterable con los ids de los componentes conectados 
     */
    public Iterable<Integer> getCC(int v)
    {
    	int idC = id[v];
		Bag<Integer> rta = new Bag<Integer>();
		for (int i = 0; i < id.length; i++)
			if(id[i]==idC)
				rta.add(i);
		return rta;
    }

    /**
     * Returns true if vertices {@code v} and {@code w} are in the same
     * connected component.
     *
     * @param  v one vertex
     * @param  w the other vertex
     * @return {@code true} if vertices {@code v} and {@code w} are in the same
     *         connected component; {@code false} otherwise
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     * @throws IllegalArgumentException unless {@code 0 <= w < V}
     */
    public boolean connected(int v, int w) {
        validateVertex(v);
        validateVertex(w);
        return id(v) == id(w);
    }

    /**
     * Returns true if vertices {@code v} and {@code w} are in the same
     * connected component.
     *
     * @param  v one vertex
     * @param  w the other vertex
     * @return {@code true} if vertices {@code v} and {@code w} are in the same
     *         connected component; {@code false} otherwise
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     * @throws IllegalArgumentException unless {@code 0 <= w < V}
     * @deprecated Replaced by {@link #connected(int, int)}.
     */
    public boolean areConnected(int v, int w) {
        validateVertex(v);
        validateVertex(w);
        return id(v) == id(w);
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        int V = marked.length;
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }
    
    public LinkedList<Vertice> getVerticesOnComponent(int componentId)
    {
    	return componentes[componentId];
    }
    
    public LinkedList<WeighedEdge> getEdgesOnComponent(int componentId)
    {
    	return relaciones[componentId];
    }
}
   

/**Copyright © 2000–2019, Robert Sedgewick and Kevin Wayne.
 * Last updated: Sat Nov 16 05:50:17 EST 2019.
 */
