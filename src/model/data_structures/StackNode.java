package model.data_structures;

public class StackNode<T> {
	
	private StackNode<T> next;

	private T object;

	public StackNode(T pObject) 
	{
		next = null;
		object = pObject;
	}

	public StackNode<T> getNext() 
	{
		return next;}

	public void setNext ( StackNode<T> pNext) 
	{
		next = pNext;
	}

	public T getObject()
	{
		return object;
	}

	public void setObject (T pObject) 
	{
		object = pObject;
	}

}
