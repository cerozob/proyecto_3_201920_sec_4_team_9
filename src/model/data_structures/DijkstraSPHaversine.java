package model.data_structures;

public class DijkstraSPHaversine {

	private double[] distTo;          // distTo[v] = distance  of shortest s->v path
    private WeighedEdge[] edgeTo;            // edgeTo[v] = last edge on shortest s->v path
    private IndexMinPQ<Double> pq;    // priority queue of vertices

    // 
    public DijkstraSPHaversine(Graph G, int s) {
        for (WeighedEdge e : (Bag<WeighedEdge>)G.edges()) {
            if (e.haver() < 0)
                throw new IllegalArgumentException("edge " + e + " has negative weight");
        }

        distTo = new double[G.V()];
        edgeTo = new WeighedEdge[G.V()];

        validateVertex(s);

        for (int v = 0; v < G.V(); v++)
            distTo[v] = Double.POSITIVE_INFINITY;
        distTo[s] = 0.0;

        // relax vertices in order of distance from s
        pq = new IndexMinPQ<Double>(G.V());
        pq.insert(s, distTo[s]);
        while (!pq.isEmpty()) {
            int v = pq.delMin();
            for (WeighedEdge e : (Bag<WeighedEdge>)G.adj(v))
                relax(e, v);
        }
    }

    private void relax(WeighedEdge e, int v) {
        int w = e.other(v);
        if (distTo[w] > distTo[v] + e.haver()) {
            distTo[w] = distTo[v] + e.haver();
            edgeTo[w] = e;
            if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
            else                pq.insert(w, distTo[w]);
        }
    }

	public boolean hasPathTo(int v)
	{ return distTo[v] < Double.POSITIVE_INFINITY; }
	
	public double distTo(int v) {
        validateVertex(v);
        return distTo[v];
	}
	
	public Iterable<WeighedEdge> pathTo(int v) {
        validateVertex(v);
        if (!hasPathTo(v)) return null;
        Stack<WeighedEdge> path = new Stack<WeighedEdge>();
        int x = v;
        for (WeighedEdge e = edgeTo[v]; e != null; e = edgeTo[x]) {
            path.push(e);
            x = e.other(x);
        }
        return path;
    }
	
	private void validateVertex(int v) {
        int V = distTo.length;
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }

}

