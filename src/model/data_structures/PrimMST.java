package model.data_structures;

import model.logic.Vertice;

public class PrimMST<T> {
    private static final double FLOATING_POINT_EPSILON = 1E-12;

    private WeighedEdge[] edgeTo;        // edgeTo[v] = shortest edge from tree vertex to non-tree vertex
    private double[] distTo;      // distTo[v] = weight of shortest such edge
    private boolean[] marked;     // marked[v] = true if v on tree, false otherwise
    private IndexMinPQ<Double> pq;

    /**
     * Compute a minimum spanning tree (or forest) of an edge-weighted graph.
     * @param G the edge-weighted graph
     */
    public PrimMST(Graph G, Iterable<Vertice> vertices) {
        edgeTo = new WeighedEdge[G.V()];
        distTo = new double[G.V()];
        marked = new boolean[G.V()];
        pq = new IndexMinPQ<Double>(G.V());
        for (int v = 0; v < G.V(); v++)
            distTo[v] = Double.POSITIVE_INFINITY;

        for (Vertice vert : vertices)   // run from each vertex to find
        {   
        	int v = vert.getId();
            if (!marked[v]) prim(G, v);   
        }									// minimum spanning tree (Supuestamente)

        // check optimality conditions
        assert check(G);
    }

    // run Prim's algorithm in graph G, starting from vertex s
    private void prim(Graph G, int s) {
        distTo[s] = 0.0;
        pq.insert(s, distTo[s]);
        while (!pq.isEmpty()) {
            int v = pq.delMin();
            scan(G, v);
        }
    }

    // scan vertex v
    private void scan(Graph G, int v) {
        marked[v] = true;
        for (WeighedEdge e : (Iterable<WeighedEdge>)G.adj(v)) {
            int w = e.other(v);
            if (marked[w]) continue;         // v-w is obsolete edge
            if (e.haver() < distTo[w]) {
                distTo[w] = e.haver();
                edgeTo[w] = e;
                if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
                else                pq.insert(w, distTo[w]);
            }
        }
    }

    /**
     * Returns the edges in a minimum spanning tree (or forest).
     * @return the edges in a minimum spanning tree (or forest) as
     *    an iterable of edges
     */
    public Iterable<WeighedEdge> edges() {
        Queue<WeighedEdge> mst = new Queue<WeighedEdge>();
        for (int v = 0; v < edgeTo.length; v++) {
        	WeighedEdge e = edgeTo[v];
            if (e != null) {
                mst.enqueue(e);
            }
        }
        return mst;
    }

    /**
     * Returns the sum of the edge weights in a minimum spanning tree (or forest).
     * @return the sum of the edge weights in a minimum spanning tree (or forest)
     */
    public double weight() {
        double weight = 0.0;
        for (WeighedEdge e : edges())
            weight += e.haver();
        return weight;
    }


    // check optimality conditions (takes time proportional to E V lg* V)
    private boolean check(Graph G) {

        // check weight
        double totalWeight = 0.0;
        for (WeighedEdge e : edges()) {
            totalWeight += e.haver();
        }
        if (Math.abs(totalWeight - weight()) > FLOATING_POINT_EPSILON) {
            System.err.printf("Weight of edges does not equal weight(): %f vs. %f\n", totalWeight, weight());
            return false;
        }

        // check that it is acyclic
        UF uf = new UF(G.V());
        for (WeighedEdge e : edges()) {
            int v = e.either(), w = e.other(v);
            if (uf.find(v) == uf.find(w)) {
                System.err.println("Not a forest");
                return false;
            }
            uf.union(v, w);
        }

        // check that it is a spanning forest
        for (WeighedEdge e : (Iterable<WeighedEdge>)G.edges()) {
            int v = e.either(), w = e.other(v);
            if (uf.find(v) != uf.find(w)) {
                System.err.println("Not a spanning forest");
                return false;
            }
        }

        // check that it is a minimal spanning forest (cut optimality conditions)
        for (WeighedEdge e : edges()) {

            // all edges in MST except e
            uf = new UF(G.V());
            for (WeighedEdge f : edges()) {
                int x = f.either(), y = f.other(x);
                if (f != e) uf.union(x, y);
            }

            // check that e is min weight edge in crossing cut
            for (WeighedEdge f : (Iterable<WeighedEdge>)G.edges()) {
                int x = f.either(), y = f.other(x);
                if (uf.find(x) != uf.find(y)) {
                    if (f.haver() < e.haver()) {
                        System.err.println("Edge " + f + " violates cut optimality conditions");
                        return false;
                    }
                }
            }

        }

        return true;
    }
    
    
    
    //____________________________________________________________________________________
    //_______________________________________Clase UF_____________________________________________
    //____________________________________________________________________________________

    
    
    private class UF{
    	
    	private int[] parent;  // parent[i] = parent of i
        private byte[] rank;   // rank[i] = rank of subtree rooted at i (never more than 31)
        private int count;     // number of components
    	
    	public UF(int n) {
    		if (n < 0) throw new IllegalArgumentException();
    		count = n;
    		parent = new int[n];
    		rank = new byte[n];
    		for (int i = 0; i < n; i++) {
    			parent[i] = i;
    			rank[i] = 0;
    		}
    	}

    	/**
    	 * Returns the canonical element of the set containing element {@code p}.
    	 *
    	 * @param  p an element
    	 * @return the canonical element of the set containing {@code p}
    	 * @throws IllegalArgumentException unless {@code 0 <= p < n}
    	 */
    	public int find(int p) {
    		validate(p);
    		while (p != parent[p]) {
    			parent[p] = parent[parent[p]];    // path compression by halving
    			p = parent[p];
    		}
    		return p;
    	}

    	/**
    	 * Returns the number of sets.
    	 *
    	 * @return the number of sets (between {@code 1} and {@code n})
    	 */
    	public int count() {
    		return count;
    	}

    	/**
    	 * Returns true if the two elements are in the same set.
    	 *
    	 * @param  p one element
    	 * @param  q the other element
    	 * @return {@code true} if {@code p} and {@code q} are in the same set;
    	 *         {@code false} otherwise
    	 * @throws IllegalArgumentException unless
    	 *         both {@code 0 <= p < n} and {@code 0 <= q < n}
    	 * @deprecated Replace with two calls to {@link #find(int)}.
    	 */
    	@Deprecated
    	public boolean connected(int p, int q) {
    		return find(p) == find(q);
    	}

    	/**
    	 * Merges the set containing element {@code p} with the 
    	 * the set containing element {@code q}.
    	 *
    	 * @param  p one element
    	 * @param  q the other element
    	 * @throws IllegalArgumentException unless
    	 *         both {@code 0 <= p < n} and {@code 0 <= q < n}
    	 */
    	public void union(int p, int q) {
    		int rootP = find(p);
    		int rootQ = find(q);
    		if (rootP == rootQ) return;

    		// make root of smaller rank point to root of larger rank
    		if      (rank[rootP] < rank[rootQ]) parent[rootP] = rootQ;
    		else if (rank[rootP] > rank[rootQ]) parent[rootQ] = rootP;
    		else {
    			parent[rootQ] = rootP;
    			rank[rootP]++;
    		}
    		count--;
    	}

    	// validate that p is a valid index
    	private void validate(int p) {
    		int n = parent.length;
    		if (p < 0 || p >= n) {
    			throw new IllegalArgumentException("index " + p + " is not between 0 and " + (n-1));  
    		}
    	}
    }
 
//    Copyright © 2000–2019, Robert Sedgewick and Kevin Wayne.
//    Last updated: Sun Nov 17 09:23:32 EST 2019.   
    
}


//Copyright © 2000–2019, Robert Sedgewick and Kevin Wayne.
//Last updated: Sun Nov 17 09:33:53 EST 2019.