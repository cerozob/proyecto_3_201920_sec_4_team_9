package model.data_structures;

import java.util.Iterator;

public class LinkedList<T> implements ILinkedList<T> {

	private NodeForLinkedList<T> first;
	private NodeForLinkedList<T> last;
	private int size;

	public LinkedList()
	{
		first = null;
		last = null;
		size = 0;
	}

	public int getSize() {
		return size;
	}

	public boolean isEmpty() {
		if(size == 0)
			return true;

		return false;
	}

	public T getFirst() {
		
		return (first != null)? first.getObject(): null;
	}

	public T getLast() {
		return last.getObject();
	}

	//agregar al inicio
	public void prepend(T object) {
		if(isEmpty())
		{
			append(object);
			return;
		}
		NodeForLinkedList<T> node = new NodeForLinkedList<T>(object);
		node.setNext(first);
		node.setIndex(1);
		first = node;
		NodeForLinkedList<T> current = first;
		while(current.getNext() != null)
		{
			current.getNext().setIndex(current.getIndex()+1);
			current = current.getNext();
		}
		size++;
	}

	public void append(T object) {
		NodeForLinkedList<T> node = new NodeForLinkedList<T>(object);
		if(!isEmpty())
		{
			node.setIndex(last.getIndex()+1);
			last.setNext(node);
			last = node;
		}
		else
		{
			first = node;
			last = node;
		}
		size++;
	}

	public void removeFirst() {
		if(first == null)
			System.out.println("La lista esta vacía");
		else
		{ //Si en la lista hay un siguiente del primer nodo
			NodeForLinkedList<T> segundo = first.getNext();
			if(segundo != null)
			{
				first = segundo;
				first.setIndex(0);
				NodeForLinkedList<T> current = first;
				while(current.getNext() != null)
				{
					current.setIndex(current.getIndex()-1);
					current = current.getNext();
				}
				size--;
				last=getNode(size);
			} 
			else //Si no hay segundo nodo
			{
				last = null;
				first = null; 
				size = 0;
			}
		}
	}

	public void removeLast() {
		if(size == 0) {
			System.out.println("Lista vacia"); return;}
		else 
		{
			if(size == 1) 
			{
				first = null;
				last = null;
				size = 0;
				return;
			}
			else 
			{ 
				size = size -1;
				NodeForLinkedList<T> temp = getNode(size-1);
				temp.setNext(null);
				last = temp;
				return;
			}
		}
	}

	public void swap(int indexA, int indexB)
	{
		if(indexA != indexB)
		{
			NodeForLinkedList<T> a = getNode(indexA);
			NodeForLinkedList<T> b = getNode(indexB);
			if(indexA != 0 && indexB != 0) 
			{
				NodeForLinkedList<T> aBefore = getNode(indexA-1);
				NodeForLinkedList<T> bBefore = getNode(indexB-1);
				NodeForLinkedList<T> aAfter = a.getNext();
				NodeForLinkedList<T> bAfter = b.getNext();
				aBefore.setNext(b);
				b.setNext(aAfter);
				bBefore.setNext(a);
				a.setNext(bAfter);
				a.setIndex(indexB);
				b.setIndex(indexA);
				return;
			} else 
			{
				// Si el B es el primero
				if(indexB == 0)
				{
					//Si A es el ultimo
					if(indexA == size)
					{
						NodeForLinkedList<T> penultimo = a.getNext();
						a.setNext(b.getNext());
						first = a;
						penultimo.setNext(b);
						last = b;
						return;
					}
					// Si A no es el ultimo
					NodeForLinkedList<T> anteriorA = getNode(indexA-1);
					NodeForLinkedList<T> siguienteA = a.getNext();
					NodeForLinkedList<T> segundo = b.getNext();
					a.setNext(segundo);
					first = a;
					anteriorA.setNext(b);
					b.setNext(siguienteA);
					return;
				}
				//Si A es el primero
				if(indexA == 0)
				{
					//Si B es el ultimo
					if(indexB == size)
					{
						NodeForLinkedList<T> penultimo = b.getNext();
						b.setNext(a.getNext());
						first = b;
						penultimo.setNext(a);
						last = a;
						return;
					}
					// Si A no es el ultimo
					NodeForLinkedList<T> anteriorB = getNode(indexB-1);
					NodeForLinkedList<T> siguienteB = b.getNext();
					NodeForLinkedList<T> segundo = a.getNext();
					b.setNext(segundo);
					first = b;
					anteriorB.setNext(a);
					b.setNext(siguienteB);
					return;
				} 
				//Si A es el ultimo
				if ( indexA == size)
				{
					NodeForLinkedList<T> penultimo = getNode(indexA-1);
					//Si B es el primero
					if(indexB == 0)
					{
						NodeForLinkedList<T> segundo = b.getNext();
						a.setNext(segundo);
						a = first;
						penultimo.setNext(b);
						last = b;
						return;
					}
					//Si B no es el primero
					NodeForLinkedList <T> siguienteB = b.getNext();
					NodeForLinkedList<T> anteriorB = getNode(indexB-1);
					penultimo.setNext(b);
					last = b;
					anteriorB.setNext(a);
					a.setNext(siguienteB);
					return;
				}
				//Si B es el ultimo
				if ( indexB == size)
				{
					NodeForLinkedList<T> penultimo = getNode(indexB-1);
					//Si A es el primero
					if(indexA == 0)
					{
						NodeForLinkedList<T> segundo = a.getNext();
						b.setNext(segundo);
						b = first;
						penultimo.setNext(a);
						last = a;
						return;
					}
					//Si A no es el primero
					NodeForLinkedList <T> siguienteA = a.getNext();
					NodeForLinkedList<T> anteriorA = getNode(indexA-1);
					penultimo.setNext(a);
					last = a;
					anteriorA.setNext(b);
					b.setNext(siguienteA);
					return;
				}
			}
		}
	}


	public T get(int pIndex)
	{
		if(pIndex == 0)
			return first.getObject();
		else if(pIndex == size)
			return last.getObject();

		NodeForLinkedList<T> current = first;
		int count = 0;
		if (current != null) 
		{
			while(count < pIndex)
			{
				current = current.getNext();
				count++;
			}
			return current.getObject(); 
		}
		else
			return null;
	}

	/**
	 * //Alta complejidad, usar solo en listas cortas
	 * @param obj T objeto buscado
	 * @return true si contiene el objeto, false de lo contrario
	 */
	public boolean contains(T obj) 
	{
		NodeForLinkedList<T> x = first;
		while(x.next()!=null)
		{
			if(x.getObject().equals(obj))
				return true;
		}
		return false;
	}
	
	//_________________________________METODOS ITERADOR_______________________________________
	@Override
	public Iterator<T> iterator() {

		return new Iterator<T>()
		{
			private int current = -1;
			@Override
			public boolean hasNext() {
				if(size == 0)
				{
					return false;
				}
				else
				{
					return (current < (--size));	
				}
			}

			@Override
			public T next() {
				current++;
				return get(current);
			}
		};
	}

	public NodeForLinkedList<T> getNode(int index)
	{
		NodeForLinkedList<T> node = first;
		if(node != null)
		{
			while(node.hasNext())
			{
				if(node.getIndex() == index)
				{
					return node;
				}
				node = node.getNext();
			}
		}
		return null;
	}
}
