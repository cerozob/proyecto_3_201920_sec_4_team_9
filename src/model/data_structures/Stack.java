package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements IStack<T>, Iterable<T> {

	//Se usará la implementación con lista encadenada y nodos.
	private StackNode<T> head;
	private int size;

	public Stack()
	{
		head = null;
		size = 0;
	}

	public void push(T addObj) 
	{
		StackNode<T> add = new StackNode<T>(addObj);
		if(head == null)
		{
			head = add;
		}
		else
		{
			add.setNext(head);
			head = add;
		}
		size++;
	}


	public T pop() {
		T retornar = getFirst();
		eliminarPrimero();
		return retornar;
	}

	private void eliminarPrimero()
	{
		if(head!=null)
		{
			StackNode<T> newHead = head.getNext();
			head.setNext(null);
			head = newHead;
			size--;
		}
	}

	public int size() 
	{
		return size;
	}


	public boolean isEmpty() 
	{	
		return ( size == 0 ) ? true : false;
	}


	public T getFirst() 
	{
		if(head != null)
		{
			return head.getObject();
		}
		else
		{
			return null;
		}
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>()
		{
			@Override
			public boolean hasNext() {
				if(size() == 0)
				{
					return false;
				}
				else
				{
					return true;	
				}
			}

			@Override
			public T next() {
				return pop();
			}
		};

	}
}
