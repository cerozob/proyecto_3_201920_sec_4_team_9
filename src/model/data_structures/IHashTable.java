package model.data_structures;

import java.util.Iterator;

public interface IHashTable <K, V>
{
	public void put(K key, V val);
	public void putInSet(K key, V val);
	public V get(K key);
	public V delete(K key);
	public Iterator<V> getSet(K key);
	public Iterator<V> deleteSet(K key);
	public Iterator<K> keys();
	public int size();
	public boolean isEmpty();
	public boolean contains(K key);
	public int getM();
	public int contador();
}
