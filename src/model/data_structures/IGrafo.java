package model.data_structures;

public interface IGrafo<V> {
	
	public int V();
	public int E();
	public void addEdge(int idVertexIni,int idVertexFin, double cost);
	public V getInfoVertex(int idVertex);
	
	public void setInfoVertex(int idVertex,V infoVertex);
	public double getCostArc(int idVertexIni,int idVertexFin);
	public void setCostArc(int idVertexIni,int idVertexFin, double cost);
	public Iterable<WeighedEdge> adj (int idVertex);
	
	public void uncheck();
	public void dfs(int s);
	public int cc();
	public Iterable<Integer> getCC (int idVertex);
	void addVertex(String key,int idVertex, V infoVertex);

}
