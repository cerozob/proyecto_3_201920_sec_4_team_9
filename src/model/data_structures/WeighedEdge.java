package model.data_structures;

public class WeighedEdge implements Comparable<WeighedEdge> { 

	private final int v;
	private final int w;
	private double haver, time, speed;

	public WeighedEdge(int v, int w, double h) {
		if (v < 0) throw new IllegalArgumentException("vertex index must be a nonnegative integer");
		if (w < 0) throw new IllegalArgumentException("vertex index must be a nonnegative integer");
		if (Double.isNaN(h)) throw new IllegalArgumentException("Haver is NaN");
		this.v = v;
		this.w = w;
		this.haver = h;
		this.time = 0;
		this.speed = 0;
	}

	public WeighedEdge(int v, int w, double h, double t , double s) {
		if (v < 0) throw new IllegalArgumentException("vertex index must be a nonnegative integer");
		if (w < 0) throw new IllegalArgumentException("vertex index must be a nonnegative integer");
		if (Double.isNaN(h)) throw new IllegalArgumentException("Haver is NaN");
		if (Double.isNaN(t)) throw new IllegalArgumentException("Time is NaN");
		if (Double.isNaN(s)) throw new IllegalArgumentException("Speed is NaN");
		this.v = v;
		this.w = w;
		this.haver = h;
		this.time = t;
		this.speed = s;
	}

	public double haver() {
		return haver;
	}

	public double time() {
		return haver;
	}

	public double speed() {
		return haver;
	}

	public void setHaver(double pWeight)
	{
		haver = pWeight;
	}

	public void setTime(double pWeight)
	{
		time = pWeight;
	}

	public void setSpeed(double pWeight)
	{
		speed = pWeight;
	}
	
	public int either()
	{
		return v;
	}

	public int other(int u) {
		if(u != w)
			return w;
		else 
			return v;
	}

	public int compareToSpeed(WeighedEdge that) {
		if(speed > that.speed())
		{
			return 1;
		}
		else if(speed < that.speed())
		{
			return -1;
		}
		else
		{
			return 0;
		}	
	}

	public int compareToTime(WeighedEdge that) {
		if(time > that.time())
		{
			return 1;
		}
		else if(time < that.time())
		{
			return -1;
		}
		else
		{
			return 0;
		}	
	}

	public int compareTo(WeighedEdge that) //Deafault compareTo: Haversine cost
	{
		if(haver > that.haver())
		{
			return 1;
		}
		else if(haver < that.haver())
		{
			return -1;
		}
		else
		{
			return 0;
		}	
	}

}


//Copyright � 2000�2017, Robert Sedgewick and Kevin Wayne.
//Last updated: Fri Oct 20 12:50:46 EDT 2017.