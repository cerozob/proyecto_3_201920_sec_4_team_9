package model.data_structures;


public class KruskalMST {
	private static final double FLOATING_POINT_EPSILON = 1E-12;

	private double weight;                        // weight of MST
	private Queue<WeighedEdge> mst = new Queue<WeighedEdge>();  // edges in MST

	/**
	 * Compute a minimum spanning tree (or forest) of an edge-weighted graph.
	 * @param G the edge-weighted graph
	 */
	public KruskalMST(Graph G, Iterable<WeighedEdge> edges) {
		// more efficient to build heap by passing array of edges
		MinHeapPQ<WeighedEdge> pq = new MinHeapPQ<WeighedEdge>();
		for (WeighedEdge e : edges) {
			pq.insert(e);
		}

		// run greedy algorithm
		UF uf = new UF(G.V());
		while (!pq.isEmpty() && mst.size() < G.V() - 1) {
			WeighedEdge e = pq.delMin();
			int v = e.either();
			int w = e.other(v);
			if (uf.find(v) != uf.find(w)) { // v-w does not create a cycle
				uf.union(v, w);  // merge v and w components
				mst.enqueue(e);  // add edge e to mst
				weight += e.haver();
			}
		}

		// check optimality conditions
		assert check(G);
	}

	/**
	 * Returns the edges in a minimum spanning tree (or forest).
	 * @return the edges in a minimum spanning tree (or forest) as
	 *    an iterable of edges
	 */
	public Iterable<WeighedEdge> edges() {
		return mst;
	}

	/**
	 * Returns the sum of the edge weights in a minimum spanning tree (or forest).
	 * @return the sum of the edge weights in a minimum spanning tree (or forest)
	 */
	public double weight() {
		return weight;
	}

	// check optimality conditions (takes time proportional to E V lg* V)
	private boolean check(Graph G) {

		// check total weight
		double total = 0.0;
		for (WeighedEdge e : edges()) {
			total += e.haver();
		}
		if (Math.abs(total - weight()) > FLOATING_POINT_EPSILON) {
			System.err.printf("Weight of edges does not equal weight(): %f vs. %f\n", total, weight());
			return false;
		}

		// check that it is acyclic
		UF uf = new UF(G.V());
		for (WeighedEdge e : edges()) {
			int v = e.either(), w = e.other(v);
			if (uf.find(v) == uf.find(w)) {
				System.err.println("Not a forest");
				return false;
			}
			uf.union(v, w);
		}

		// check that it is a spanning forest
		for (WeighedEdge e : (Iterable<WeighedEdge>)G.edges()) {
			int v = e.either(), w = e.other(v);
			if (uf.find(v) != uf.find(w)) {
				System.err.println("Not a spanning forest");
				return false;
			}
		}

		// check that it is a minimal spanning forest (cut optimality conditions)
		for (WeighedEdge e : edges()) {

			// all edges in MST except e
			uf = new UF(G.V());
			for (WeighedEdge f : mst) {
				int x = f.either(), y = f.other(x);
				if (f != e) uf.union(x, y);
			}

			// check that e is min weight edge in crossing cut
			for (WeighedEdge f : (Iterable<WeighedEdge>)G.edges()) {
				int x = f.either(), y = f.other(x);
				if (uf.find(x) != uf.find(y)) {
					if (f.haver() < e.haver()) {
						System.err.println("Edge " + f + " violates cut optimality conditions");
						return false;
					}
				}
			}

		}

		return true;
	}





//____________________________________________________________________________________
//_______________________________________Clase UF_____________________________________________
//____________________________________________________________________________________



private class UF{

	private int[] parent;  // parent[i] = parent of i
	private byte[] rank;   // rank[i] = rank of subtree rooted at i (never more than 31)
	private int count;     // number of components

	public UF(int n) {
		if (n < 0) throw new IllegalArgumentException();
		count = n;
		parent = new int[n];
		rank = new byte[n];
		for (int i = 0; i < n; i++) {
			parent[i] = i;
			rank[i] = 0;
		}
	}

	/**
	 * Returns the canonical element of the set containing element {@code p}.
	 *
	 * @param  p an element
	 * @return the canonical element of the set containing {@code p}
	 * @throws IllegalArgumentException unless {@code 0 <= p < n}
	 */
	public int find(int p) {
		validate(p);
		while (p != parent[p]) {
			parent[p] = parent[parent[p]];    // path compression by halving
			p = parent[p];
		}
		return p;
	}

	/**
	 * Returns the number of sets.
	 *
	 * @return the number of sets (between {@code 1} and {@code n})
	 */
	public int count() {
		return count;
	}

	/**
	 * Returns true if the two elements are in the same set.
	 *
	 * @param  p one element
	 * @param  q the other element
	 * @return {@code true} if {@code p} and {@code q} are in the same set;
	 *         {@code false} otherwise
	 * @throws IllegalArgumentException unless
	 *         both {@code 0 <= p < n} and {@code 0 <= q < n}
	 * @deprecated Replace with two calls to {@link #find(int)}.
	 */
	@Deprecated
	public boolean connected(int p, int q) {
		return find(p) == find(q);
	}

	/**
	 * Merges the set containing element {@code p} with the 
	 * the set containing element {@code q}.
	 *
	 * @param  p one element
	 * @param  q the other element
	 * @throws IllegalArgumentException unless
	 *         both {@code 0 <= p < n} and {@code 0 <= q < n}
	 */
	public void union(int p, int q) {
		int rootP = find(p);
		int rootQ = find(q);
		if (rootP == rootQ) return;

		// make root of smaller rank point to root of larger rank
		if      (rank[rootP] < rank[rootQ]) parent[rootP] = rootQ;
		else if (rank[rootP] > rank[rootQ]) parent[rootQ] = rootP;
		else {
			parent[rootQ] = rootP;
			rank[rootP]++;
		}
		count--;
	}

	// validate that p is a valid index
	private void validate(int p) {
		int n = parent.length;
		if (p < 0 || p >= n) {
			throw new IllegalArgumentException("index " + p + " is not between 0 and " + (n-1));  
		}
	}
}

//Copyright © 2000–2019, Robert Sedgewick and Kevin Wayne.
//Last updated: Sun Nov 17 09:23:32 EST 2019.   


}


//Copyright © 2000–2019, Robert Sedgewick and Kevin Wayne.
//Last updated: Sun Nov 17 09:33:53 EST 2019.