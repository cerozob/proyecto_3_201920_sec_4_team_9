package model.logic;

public class Punto {
	
	private double longitud;
	private double latitud;
	
	public Punto(double pLongitud, double pLatitud) {
		longitud = pLongitud;
		latitud = pLatitud;
	}
	
	public double getLongitud() {	return longitud;	}
	
	public double getLatitud() {	return latitud;		}
	
	public double[] getPunto()
	{
		double punto[] = {longitud,latitud};
		return punto;
	}
}
