package model.logic;

public class Vertice implements Comparable<Vertice>
{

	private int id;
	private Punto coordenadas;
	private int mov_Id;
	
	public Vertice(int pId,double pLongitud, double pLatitud, int pMov_Id)
	{
		id = pId;
		coordenadas = new Punto(pLongitud, pLatitud);
		mov_Id = pMov_Id;
	}

	public Vertice(int pId,Punto pPunto)
	{
		id = pId;
		coordenadas = pPunto;
	}
	
	public int getId() {
		return id;
	}

	public Punto getCoordenadas() {
		return coordenadas;
	}
	
	public double getLongitud()
	{
		return coordenadas.getLongitud();
	}
	
	public double getLatitud()
	{
		return coordenadas.getLatitud();
	}

	@Override
	public int compareTo(Vertice nodo) {
		if(nodo.getId() < id)
		{
			return 1;
		}
		else if (id < nodo.getId())
		{
			return -1;
		}
		else if (nodo.getId() == id)
		{
			return 0;
		}
		return 698698;
	}

	public int getMov_Id() {
		return mov_Id;
	}
}
