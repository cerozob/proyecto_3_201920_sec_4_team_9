package model.logic;

public class TravelTime implements Comparable<TravelTime> {
	
	public enum TIMETYPE{
		hod,
		month,
		dow
	}
	
	private int trimestre;
	//id del punto de partida del viaje
	private int sourceid;

	//id del punto de destino
	private int dstid;

	//tiempo del viaje, dependiente dl tipo de la enumeraci�n
	private int hod;

	//tiempo promedio del viaje
	private double mean_travel_time;

	//desviaci�n estandar el tiempo de viaje
	private double standard_deviation_travel_time;
	
	//Hash code
	private int hash;
	
	//tipo del tiempo, hod para hora, dow para dia de la semana y month para mes
	private TIMETYPE timeType;

	public TravelTime(int pSourceid,int pDstid,int pTime,double pMean_travel_time,double pStandard_deviation_travel_time,double pGeometric_mean_travel_time,double pGeometric_standard_deviation_travel_time,int pTrimestre,TIMETYPE pTimeType)
	{
		sourceid = pSourceid;
		dstid = pDstid;
		hod = pTime;
		mean_travel_time = pMean_travel_time;
		standard_deviation_travel_time = pStandard_deviation_travel_time;
		trimestre = pTrimestre;
		timeType = pTimeType;
		hash = -1;
	}

	public int getSourceid() {
		return sourceid;
	}

	public int getDstid() {
		return dstid;
	}

	public int getTime() {
		return hod;
	}

	public double getMean_travel_time() {
		return mean_travel_time;
	}

	public double getStandard_deviation_travel_time() {
		return standard_deviation_travel_time;
	}

	public int getTrimestre()
	{
		return trimestre;
	}
	
	public TIMETYPE getTimeType()
	{
		return timeType;
	}
	
	@Override
	public int compareTo(TravelTime comparar) {
		// compara dos objetos de tipo UberData por el tiempo promedio de viaje,
		//retorna -1 si el objeto por par�metro es mayor, 1 en caso contrario y 0 si son iguales.
			if (mean_travel_time < comparar.getMean_travel_time())
				return -1;
			else if(mean_travel_time > comparar.getMean_travel_time())
				return 1;
			return 0;
	}
	
	public int hashCode()
	{
		if (hash < 0) 
		{
			String s = trimestre + "-" + sourceid + "-"+ dstid;
			int hash = 0;
			for (int i = 0; i < s.length(); i++)
				hash = 31 * hash + s.charAt(i);
		} 
		return hash;
	}
}