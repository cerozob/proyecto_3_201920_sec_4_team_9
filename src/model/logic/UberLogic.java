package model.logic;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.Iterator;

import model.data_structures.*;
import model.logic.TravelTime.TIMETYPE;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.opencsv.CSVReader;

/**
 * Definicion del modelo del mundo
 *
 */
public class UberLogic {

	/**
	 * Atributos del modelo del mundo
	 */

	/**
	 *Ayuda: Puede usar como llave un String que sea la concatenaci�n de trimestre,
	 *sourceid y dstid separados por �-�.
	 */

	public final int MAX_CONSOLA = 20;

	private Graph<Vertice> graph;
	private SeparateChainingHashST<String, TravelTime> htTravelTime;
	
	
	/**
	 * Atributos de algoritmos
	 */
	private CC cc;

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public UberLogic(String pRutaArchivoVertices)
	{
		graph = new Graph<Vertice>(darNumVertices(pRutaArchivoVertices));
		htTravelTime = new SeparateChainingHashST<String, TravelTime>(2);
		
	}


	private int darNumVertices(String pRutaArchivo) {
		int mayor = 0;
		try {
			BufferedReader br = new BufferedReader(new FileReader(pRutaArchivo));
			String linea;
			br.readLine();
			while((linea = br.readLine()) != null)
			{
				int id = Integer.parseInt(linea.split(";")[0]);
				if(id >= mayor)
				{
					mayor = id;
				}
			}
			br.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
		return mayor+1;
	}


	public int loadVertexTXT(String pRutaArchivo)
	{
		int contador = 0;
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(pRutaArchivo)));
			String actual;
			br.readLine();
			while((actual = br.readLine())!= null)
			{
				String[] linea = actual.split(";");
				double longi = Double.parseDouble(linea[1]);
				double lati = Double.parseDouble(linea[2]);
				int id = Integer.parseInt(linea[0]);
				Vertice v = new Vertice(id,
						longi,
						lati ,Integer.parseInt(linea[3]));
				String key = longi + "-" + lati;
				graph.addVertex(key,id , v);
				contador++;
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contador;
	}

	public int loadLinksTXT(String pRutaArchivo)
	{
		int contador = 0;
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(pRutaArchivo)));
			String actual;
			while((actual = br.readLine())!= null)
			{
				String[] linea = actual.split(" ");
				Vertice pA = graph.getInfoVertex(Integer.parseInt(linea[0]));
				for(int i = 1;i<linea.length;i++)
				{
					Vertice pB = graph.getInfoVertex(Integer.parseInt(linea[i]));
					if(pA == null || pB == null)
					{
						continue;
					}
					double pCostoH = Haversine.distancia(pA, pB);
					double pCostoT = 0;
					if(pA.getMov_Id() == pB.getMov_Id())
					{
						pCostoT = calcularTiempoPromedioZonasIguales(pA.getMov_Id());
						if(pCostoT == 0)
						{
							pCostoT = 10;
						}
					}
					else
					{
						pCostoT = calcularTiempoPromedio(pA.getMov_Id(), pB.getMov_Id());
						if(pCostoT == 0)
						{
							pCostoT = 100;
						}
					}
					double pCostoS = pCostoH/pCostoT;
					WeighedEdge w = new WeighedEdge(pA.getId(), pB.getId(), pCostoH, pCostoT, pCostoS);
					graph.addEdge(w);
					contador++;
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contador;
	}


	public void loadGraph(String pRutaArchivoEdges,String pRutaArchivoVertices)
	{
		loadVertexTXT(pRutaArchivoVertices);
		loadLinksTXT(pRutaArchivoEdges);
		try {
		ejecutarAlgoritmos();}
		catch(Exception e)
		{
			System.out.println("Falla CC");
			e.printStackTrace();
		}
	}

	//clase comparable auxiliar
	public class ObjetoComparableAuxiliar implements Comparable<ObjetoComparableAuxiliar>{
		private double entero;
		private String cadena;

		public ObjetoComparableAuxiliar(double pEntero, String pCadena)
		{
			entero = pEntero;
			cadena = pCadena;
		}

		@Override
		public int compareTo(ObjetoComparableAuxiliar o) {

			if(entero > o.entero)
			{
				return 1;
			}
			else if(entero < o.entero)
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}

		public double darEntero()
		{
			return entero;
		}
		public String darCadena()
		{
			return cadena;
		}

	}

	public void writeGraph(String pRutaArchivo) throws Exception
	{
		File archivo = new File(pRutaArchivo);
		if(!archivo.exists())
		{
			archivo.createNewFile();
		}
		Iterator<WeighedEdge> iterEdges = graph.edges().iterator();
		Gson gson = new Gson();
		JsonWriter writer = gson.newJsonWriter(new FileWriter(archivo));
		writer.beginArray();
		writer.beginObject();
		writer.name("Edges");
		writer.beginArray();
		while(iterEdges.hasNext())
		{
			writer.beginObject();
			WeighedEdge w = iterEdges.next();
			writer.name("nodoA").value(w.either());
			writer.name("nodoB").value(w.other(w.either()));
			writer.name("haversine").value(w.haver());
			writer.name("time").value(w.time());
			writer.name("speed").value(w.speed());
			writer.endObject();
		}
		writer.endArray();
		writer.endObject();

		writer.beginObject();
		writer.name("Vertices");
		writer.beginArray();

		Iterator<Vertice> iterVertices = graph.getVertices().iterator();
		while(iterVertices.hasNext())
		{
			writer.beginObject();
			Vertice v = iterVertices.next();
			writer.name("id").value(v.getId());
			writer.name("longitud").value(v.getLongitud());
			writer.name("latitud").value(v.getLatitud());
			writer.name("MOVEMENT_ID").value(v.getMov_Id());
			writer.endObject();
		}
		writer.endArray();
		writer.endObject();
		writer.endArray();
		writer.close();
	}

	public String loadGraphFromJson(String pRutaArchivo)
	{
		File archivo = new File(pRutaArchivo);
		Gson gson = new Gson();
		JsonReader reader;
		try {
			reader = gson.newJsonReader(new FileReader(archivo));
			reader.beginArray();
			reader.beginObject();
			reader.nextName();
			reader.beginArray();
			while(reader.hasNext())
			{
				reader.beginObject();
				int nodoA = 0;
				int nodoB = 0;
				double haver = 0.0;
				double time = 0.0;
				double speed = 0.0;
				while(reader.hasNext())
				{
					String property = reader.nextName();
					switch (property) 
					{
					case "nodoA":
						nodoA = reader.nextInt();
						break;
					case "nodoB":
						nodoB = reader.nextInt();
						break;
					case "haversine":
						haver = reader.nextDouble();
						break;
					case "time":
						time = reader.nextDouble();
						break;
					case "speed":
						speed = reader.nextDouble();
						break;
					default:
						break;
					}
				}

				WeighedEdge w = new WeighedEdge(nodoA, nodoB, haver, time, speed);
				graph.addEdge(w);

				reader.endObject();
			}
			reader.endArray();
			reader.endObject();

			reader.beginObject();
			reader.nextName();

			reader.beginArray();
			while(reader.hasNext())
			{
				reader.beginObject();
				int pId = 0;
				double pLongitud = 0.0;
				double pLatitud = 0.0;
				int pMov_Id = 0;
				try {
					while(reader.hasNext() )
					{
						switch (reader.nextName()) 
						{
						case "id":
							pId = reader.nextInt();
							break;
						case "longitud":
							pLongitud = reader.nextDouble();
							break;
						case "latitud":
							pLatitud = reader.nextDouble();
							break;
						case "MOVEMENT_ID":
							pMov_Id = reader.nextInt();
							break;
						default:
							break;
						}
					}

				}catch(Exception e)
				{

				}

				String key = pLongitud+"-"+pLatitud;

				Vertice v = new Vertice(pId, pLongitud, pLatitud, pMov_Id);
				graph.addVertex(key,pId,v);
				reader.endObject();
			}
			reader.endArray();
			reader.endObject();
			reader.endArray();
			reader.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return "Grafo cargado con "+graph.E()+" bordes y "+graph.V()+" vertices.\n la tabla de hash 'Vertices' tiene "+graph.getCantidadVertices()+" vertices y la tabla de hash 'Ubicaciones' tiene "+graph.getUbicaciones()+" vertices";
	}

	public int loadCSV(String pRutaArchivo)
	{
		int contadorLineas = 0;
		try {
			//Reader que lee el csv linea por linea
			CSVReader reader = new CSVReader(new FileReader(pRutaArchivo));
			//retorna un arreglo de arreglos de strings
			for(String[] linea : reader) 
			{
				if(contadorLineas != 0) //cuando es 0 est� leyendo la primera l�nea.
				{	
					//el �ndice del nodo empieza en 1
					htTravelTime.put(linea.toString(),new TravelTime(
							Integer.parseInt(linea[0]),
							Integer.parseInt(linea[1]), 
							Integer.parseInt(linea[2]), 
							Double.parseDouble(linea[3]), 
							Double.parseDouble(linea[4]),
							Double.parseDouble(linea[5]),
							Double.parseDouble(linea[6]),1,TIMETYPE.dow));
				}
				contadorLineas++;
			}
			reader.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
		return --contadorLineas;
	}
	
	//____________________________________________Requerimientos_______________________________________

	private void ejecutarAlgoritmos()
	{
		cc = new CC(graph);
	}
	
	//___________________________________________ PARTE A ____________________________________________________________

	//4
	public String darCostoMinimoTiempoPromedioAAAA(int longOrigen, int latOrigen, int longDest, int latDest)
	{
		Vertice origen = graph.getVertex(latOrigen,longOrigen);
		Vertice destino = graph.getVertex(latDest,longDest);
		int idOrigen = origen.getId();
		int idDest = destino.getId();
		DijkstraSPMeanTime dijMeanTime = new DijkstraSPMeanTime(graph,origen.getId());
		Stack<WeighedEdge> path = (Stack<WeighedEdge>)dijMeanTime.pathTo(destino.getId());
		double cost = dijMeanTime.distTo(destino.getId());
		String rta = "Camino del vertice "+idOrigen+ "("+latOrigen+" , "+longOrigen+") al vertice " + idDest + "("+latDest+" , "+longDest+ ") \n";
		int pasado = 0;
		for (WeighedEdge e : path)
		{
			int temp = e.either();
			if( temp == pasado ) 
				temp = e.other(temp);
			Vertice v = graph.getVertex(temp);
			 rta += "Id: "+ v.getId() + "  Latitud: " + v.getLatitud() + "  Longitud "+ v.getLongitud() + "\n";
			 pasado = temp;
		}
		rta += "Costo total: "+ cost +"\n" + "Total vertices "+ path.size();
		return rta;
	}
	//2
	public Vertice redondearVertice(double longitud, double latitud)  //TODO: econtrar el mas cercano a ciertas coordenadas.
	{
		Vertice masCercano = null;
		Double haversineDistance = 0.0;
		for(Vertice v:graph.getVertices())
		{
			double haverActual = Haversine.distance(latitud, longitud, v.getLatitud(), v.getLongitud());

			if(masCercano != null)
			{
				if(haverActual < haversineDistance)
				{
					masCercano = v;
					haversineDistance = haverActual;
				}
			}
			else
			{
				masCercano = v;
				haversineDistance = haverActual;
			}
		}
		return masCercano;
	}
	//___________________________________________ PARTE A ____________________________________________________________

	//4
	public Graph<Vertice> darCostoMinimoTiempoPromedio(int lat1, int lat2, int long1, int long2)
	{
		Vertice origen = redondearVertice(long1, lat1);
		Vertice destino = redondearVertice(long2, lat2);
		SeparateChainingHashST<Integer,Vertice> vertices = new SeparateChainingHashST<Integer,Vertice>(2);
		Stack<WeighedEdge> edges = new Stack<WeighedEdge>();
		DijkstraSPMeanTime dij = new DijkstraSPMeanTime(graph,origen.getId());
		if(dij.hasPathTo(destino.getId()))
		{
			for(WeighedEdge w:dij.pathTo(destino.getId()))
			{
				edges.push(w);
				Vertice v = graph.getInfoVertex(w.either());
				Vertice x = graph.getInfoVertex(w.other(w.either()));
				vertices.put(v.getId(), v);
				vertices.put(x.getId(), x);
			}
			Graph<Vertice> g = new Graph<Vertice>(vertices.size());
			for(WeighedEdge actual:edges)
			{
				g.addEdge(actual);
			}
			return g;
		}
		return null;
	}


	//5
	public String darNVerticesConMinimaVelocidadPromedio(int N)
	{
		String rta = "Los "+ N +" vertices con velocidad promedio mínima son: \n";
		Iterable<Vertice> vertices = graph.getVertices();
		MinHeapPQ<ObjetoComparableAuxiliar> heap = new MinHeapPQ<ObjetoComparableAuxiliar>();
		LinkedList<Integer> listaCCs = new LinkedList<Integer>();
		for (Vertice v : vertices)
		{
			Bag<WeighedEdge> adj = (Bag<WeighedEdge>)graph.adj(v.getId());
			double vel = 0;
			double tot = new Double(adj.size());
			for(WeighedEdge e : adj)
				vel += e.speed();
			vel =vel/tot;
			String info = "Id: " +v.getId()+" Latitud: " +v.getLatitud()+ " Longitud " + v.getLongitud();
			ObjetoComparableAuxiliar aux = new ObjetoComparableAuxiliar(vel, info);
			heap.insert(aux);
		}
		for (int i = 0; i < N; i++) {
			String info = heap.delMin().darCadena();
			String[] pals = info.split(" ");
			int id = Integer.parseInt(pals[1]);
			//Componentes conectados
			int idCC = cc.id(id);
			if(!listaCCs.contains(idCC))
				listaCCs.append(idCC);
			rta += info + "\n";
		}
		rta += "El total de componentes conectados que forman estos vértices es " + listaCCs.getSize()+"\n";
		for(int c : listaCCs)
		{
			rta+="Vertices del componente "+c+": \n";
			for(int vert : cc.getCC(c))
				rta += vert + ", ";
		}
		return rta;
	}


	//6
	public String darMSTPrimHaversine() 
	{
		String rta = "MST según la distancia Haversine con algoritmo Prim \n";
		//Hallar cc más grande
		int totCCs = cc.count();
		int mayorCC = 0;
		for(int i = 0; i <= totCCs; i++)
			if(mayorCC<cc.size(i))
				mayorCC = cc.size(i);
		LinkedList<Vertice> vertices = cc.getVerticesOnComponent(mayorCC);
		rta +="Total de vértices en el componente conectado mayor: "+ vertices.getSize()+ "\n";
		//MST
		long ti = System.currentTimeMillis();
		PrimMST prim = new PrimMST<Vertice>(graph,vertices);
		long tf = System.currentTimeMillis();
		long time = (tf-ti);
		rta += "Tiempo de ejecución del algoritmo: " + time + "\n";
		Queue<WeighedEdge> mst = (Queue<WeighedEdge>)prim.edges();
		rta += "El total de vertices en el MST es: " + (mst.size()/2) + "\n Los vertices que lo conforman los lo siguientes \n";
		for (Vertice v : vertices) {
			rta +=" "+ v.getId() + " · ";
		}
		rta += " \n Los arcos incluidos en el MST son: \n";
		for(WeighedEdge w : mst)
		{
			rta += w.either() + " -- " +w.other(w.either()) + "\n";
		}
		rta += "EL costo total del MST es: " + prim.weight();
		return rta;
	}
	


	//___________________________________________ PARTE B ____________________________________________________________


	//7
	public String darCostoMinimoHaversine(int longOrigen, int latOrigen, int longDest, int latDest)
	{
		Vertice origen = graph.getVertex(longOrigen,latOrigen);
		Vertice destino = graph.getVertex(longDest,latDest);
		int idOrigen = origen.getId();
		int idDest = destino.getId();
		DijkstraSPHaversine dijHaver = new DijkstraSPHaversine(graph,origen.getId());
		Stack<WeighedEdge> path = (Stack<WeighedEdge>)dijHaver.pathTo(destino.getId());
		double cost = dijHaver.distTo(destino.getId());
		String rta = "Camino del vertice "+idOrigen+ "("+latOrigen+" , "+longOrigen+") al vertice " + idDest + "("+latDest+" , "+longDest+ ") \n";
		int pasado = 0;
		for (WeighedEdge e : path)
		{
			int temp = e.either();
			if( temp == pasado ) 
				temp = e.other(temp);
			Vertice v = graph.getVertex(temp);
			 rta += "Id: "+ v.getId() + "  Latitud: " + v.getLatitud() + "  Longitud "+ v.getLongitud() + "\n";
			 pasado = temp;
		}
		rta += "Costo total: "+ cost +"\n" + "Total vertices "+ path.size();
		return rta;
	}

	//8 	
	public Graph<Vertice> darCostoMinimoDistancia(int lat1, int lat2, int long1, int long2)
	{

		Vertice origen = redondearVertice(long1, lat1);
		Vertice destino = redondearVertice(long2, lat2);
		SeparateChainingHashST<Integer,Vertice> vertices = new SeparateChainingHashST<Integer,Vertice>(2);
		Stack<WeighedEdge> edges = new Stack<WeighedEdge>();
		DijkstraSPHaversine dij = new DijkstraSPHaversine(graph,origen.getId());
		if(dij.hasPathTo(destino.getId()))
		{
			for(WeighedEdge w:dij.pathTo(destino.getId()))
			{
				edges.push(w);
				Vertice v = graph.getInfoVertex(w.either());
				Vertice x = graph.getInfoVertex(w.other(w.either()));
				vertices.put(v.getId(), v);
				vertices.put(x.getId(), x);
			}
			Graph<Vertice> g = new Graph<Vertice>(vertices.size());
			for(WeighedEdge actual:edges)
			{
				g.addEdge(actual);
			}
			return g;
		}
		return null;
	}

	//8
	public Stack<Vertice> verticesAlcanzables(double longitud,double latitud,double tiempo)
	{
		Vertice origen = redondearVertice(longitud,latitud);
		DijkstraSPMeanTime dij = new DijkstraSPMeanTime(graph, origen.getId());
		Stack<Vertice> alcanzables = new Stack<Vertice>();
		for(Vertice v :graph.getVertices())
		{
			if(dij.distTo(v.getId()) <= tiempo)
			{
				alcanzables.push(v);
			}
		}
		return alcanzables;
	}



	//9
	public String darMSTKruskalHaversine() 
	{
		String rta = "MST según la distancia Haversine con algoritmo Kruskal \n";
		//Hallar cc más grande
		int totCCs = cc.count();
		int mayorCC = 0;
		for(int i = 0; i <= totCCs; i++)
			if(mayorCC<cc.size(i))
				mayorCC = cc.size(i);
		LinkedList<Vertice> vertices = cc.getVerticesOnComponent(mayorCC);
		LinkedList<WeighedEdge> edges = cc.getEdgesOnComponent(mayorCC);
		rta +="Total de vértices en el componente conectado mayor: "+ vertices.getSize()+ "\n";
		//MST
		long ti = System.currentTimeMillis();
		KruskalMST krus = new KruskalMST(graph,edges);
		long tf = System.currentTimeMillis();
		long time = (tf-ti);
		rta += "Tiempo de ejecución del algoritmo: " + time + "\n";
		Queue<WeighedEdge> mst = (Queue<WeighedEdge>)krus.edges();
		rta += "El total de vertices en el MST es: " + (mst.size()/2) + "\n";
		rta += "Los arcos incluidos en el MST son: \n";
		for(WeighedEdge w : edges)
		{
			rta += w.either() + " -- " +w.other(w.either());
		}
		rta += "EL costo total del MST es: " + krus.weight();
		return rta;
	}

	public KruskalMSTHaversine<Vertice> calcularMSTHaversineKruskal()
	{
		CC cc = new CC(graph);

		int idVertice = 0;
		int cantidadVertices= 0;
		for(Vertice v:graph.getVertices())
		{
			if(cc.size(v.getId()) > cantidadVertices)
			{
				idVertice = v.getId();
				cantidadVertices = cc.size(idVertice);
			}
		}
		Graph<Vertice> mayorCC = new Graph<Vertice>(cantidadVertices);
		for(int i:cc.getCC(idVertice))
		{
			Vertice v = graph.getInfoVertex(i);
			for(WeighedEdge w:graph.adj(v.getId()))
			{
				if(mayorCC.getCostArcHaver(w.either(), w.other(w.either())) == 0)
				{
					mayorCC.addEdge(w);
				}
			}
			mayorCC.addVertex(v.getLongitud()+"-"+v.getLatitud(),i,v);
		}

		KruskalMSTHaversine<Vertice> kruskal = new KruskalMSTHaversine<Vertice>(mayorCC);
		return kruskal;
	}

	//___________________________________________ PARTE C ____________________________________________________________

	//10
	public Graph<VerticeZona> crearGrafoZonas()
	{
		String movementIds = "";
		Stack<VerticeZona> zonas = new Stack<VerticeZona>();
		Iterator<Vertice> iter = graph.getVertices().iterator();
		while(iter.hasNext())
		{
			Vertice actual = iter.next();
			String movId = actual.getMov_Id()+"";
			if(!(movementIds.contains(movId)))
			{
				movementIds += ","+movId+",";
				VerticeZona zona = new VerticeZona(actual.getMov_Id(), actual);
				zonas.push(zona);
			}
		}

		String[] movIdsConcatenados = movementIds.split(",");
		int contadorZonas=0;
		for(String actual:movIdsConcatenados)
		{
			if(!actual.equals(""))
			{
				contadorZonas++;
			}
		}

		Graph<VerticeZona> g = new Graph<VerticeZona>(contadorZonas);

		for(VerticeZona zona:zonas) 
		{
			String key = zona.getReferencia().getLongitud()+"-"+zona.getReferencia().getLatitud();
			g.addVertex(key, zona.getMov_Id(), zona);
		}

		Iterator<WeighedEdge> iterEdges = graph.edges().iterator();
		while(iterEdges.hasNext())
		{
			WeighedEdge actual = iterEdges.next();
			int a = actual.either();
			int b = actual.other(a);
			Vertice vA = graph.getInfoVertex(a);
			Vertice vB = graph.getInfoVertex(b);
			if(vA.getMov_Id() != vB.getMov_Id())
			{
				double peso = calcularTiempoPromedio(vA.getMov_Id(),vB.getMov_Id());
				double weight = (peso == 0)? 200 : peso;
				WeighedEdge agregar = new WeighedEdge(vA.getMov_Id(), vB.getMov_Id(), 0, weight, 0);
				boolean repetido = false;
				for(WeighedEdge edgeActual:g.adj(vA.getMov_Id()))
				{
					if( edgeActual.either() == agregar.either() && 
							edgeActual.other(edgeActual.either()) == agregar.other(agregar.either()) 
							|| 
							edgeActual.either() == agregar.other(agregar.either()) && 
							edgeActual.other(edgeActual.either()) == agregar.either())
					{
						repetido = true;
					}
				}

				if(!repetido)
				{
					g.addEdge(agregar);
				}
			}
		}
		return g;
	}


	private double calcularTiempoPromedio(int mov_Id1, int mov_Id2) {

		Stack<TravelTime> pila = new Stack<TravelTime>();
		Iterator<String> iter = htTravelTime.keys();
		while(iter.hasNext())
		{
			TravelTime actual = htTravelTime.get(iter.next());
			if(actual.getSourceid() == mov_Id1 || actual.getSourceid() == mov_Id2 && actual.getDstid() == mov_Id1 ||actual.getDstid() == mov_Id2 && actual.getDstid() != actual.getSourceid())
			{
				pila.push(actual);
			}
		}
		double avg = 0.0;
		double contador = 0;
		double sumatoria = 0;
		for(TravelTime actual:pila)
		{
			sumatoria += actual.getTime();
			contador++;
		}
		avg = (contador == 0)? 0 : sumatoria/contador;
		return avg;
	}

	private double calcularTiempoPromedioZonasIguales(int mov_Id1) {

		Stack<TravelTime> pila = new Stack<TravelTime>();
		Iterator<String> iter = htTravelTime.keys();
		while(iter.hasNext())
		{
			TravelTime actual = htTravelTime.get(iter.next());
			if(actual.getSourceid() == mov_Id1 && actual.getDstid() == mov_Id1)
			{
				pila.push(actual);
			}
		}
		double avg = 0.0;
		double contador = 0;
		double sumatoria = 0;
		for(TravelTime actual:pila)
		{
			sumatoria += actual.getTime();
			contador++;
		}
		avg = (contador == 0)? 0 : sumatoria/contador;
		return avg;
	}

	//11
	
	public String calcularCostoMinZonas(int Mov_id)
	{
		Graph<VerticeZona> g = crearGrafoZonas();
		String rta;
		long ti = System.currentTimeMillis();
		DijkstraSPMeanTime dij = new DijkstraSPMeanTime(g, Mov_id);
		long tf = System.currentTimeMillis();
		long time = (tf-ti);
		rta += "Tiempo de ejecución del algoritmo: " + time + "\n";
		
		rta += "El total de vertices en el MST es: " + "\n";
		rta += "Los arcos incluidos en el MST son: \n";
		return rta;
		
	}
	//12
}	

