package model.logic;

public class VerticeZona {

	private int mov_Id;
	private Vertice referencia;
	
	public VerticeZona(int pMov_Id,Vertice pReferencia)
	{
		mov_Id = pMov_Id;
		referencia = pReferencia;
	}

	public int getMov_Id() {
		return mov_Id;
	}

	public Vertice getReferencia() {
		return referencia;
	}
}
