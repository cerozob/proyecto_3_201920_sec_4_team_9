package controller;

import java.util.Scanner;

import model.data_structures.Bag;
import model.data_structures.Graph;
import model.data_structures.WeighedEdge;
import model.logic.UberLogic;
import model.logic.Vertice;
import model.logic.VerticeZona;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private UberLogic modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new UberLogic("./data/bogota_vertices.txt");
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("Cargar Grafo");
				modelo.loadGraph("./data/bogota_arcos.txt","./data/bogota_vertices.txt");
				modelo.loadCSV("./data/bogota-cadastral-2018-1-WeeklyAggregate.csv");
				break;
			case 2:
				System.out.println("Cargar Grafo Desde JSON");
				String cargar= modelo.loadGraphFromJson("./data/bogota_grafo.json");
				System.out.println(cargar);
				break;

			case 3:
				System.out.println("Guardar grafo a JSON");
				try {
					modelo.writeGraph("./data/bogota_grafo.json");
				} catch (Exception e) {
					e.printStackTrace();
				}

				break;

			case 4:
				System.out.println("Crear grafo de zonas uber");
				Graph<VerticeZona> g = modelo.crearGrafoZonas();
				System.out.println("el grafo tiene: "+g.V()+" v�rtices(zonas) y "+g.E()+" arcos");
				break;

				//
			case 5: 
				System.out.println("Encontrar camino menor tiempo promedio según Uber");
				System.out.println("Ingrese latitud de la zona de origen");
				int latorigen1 = lector.nextInt();
				System.out.println("Ingrese longitud de la zona de origen");
				int longiorigen1 = lector.nextInt();
				System.out.println("Ingrese latitud de la zona de destino");
				int latidest1 = lector.nextInt();
				System.out.println("Ingrese longitud de la zona de destino");
				int longidest1 = lector.nextInt();
				Graph<Vertice> gr = modelo.darCostoMinimoTiempoPromedio(latorigen1, latidest1, longiorigen1, longidest1);
				Bag<WeighedEdge> bag =  (Bag<WeighedEdge>)gr.edges();
				System.out.println("Total");
				for(WeighedEdge w : bag)
					System.out.println(w.either() +" - " + w.other(w.either()));
				break;	
				
			case 6: 
				System.out.println("Encontrar camino menor tiempo promedio según Uber (opcion2)");
				System.out.println("Ingrese latitud de la zona de origen");
				int latorigen11 = lector.nextInt();
				System.out.println("Ingrese longitud de la zona de origen");
				int longiorigen11 = lector.nextInt();
				System.out.println("Ingrese latitud de la zona de destino");
				int latidest11 = lector.nextInt();
				System.out.println("Ingrese longitud de la zona de destino");
				int longidest11 = lector.nextInt();
				System.out.println(modelo.darCostoMinimoTiempoPromedioAAAA(longiorigen11, latorigen11, longidest1, latidest11));
				System.out.println();
				break;	

			case 7:
				System.out.println("Determinar los N vertices con menore velocidades");
				int N = lector.nextInt();
				System.out.println(modelo.darNVerticesConMinimaVelocidadPromedio(N));
				break;
				
			case 8:
				System.out.println("Calcular	 un	 árbol	 de	 expansión	 mínima	 (MST)	 con	 criterio	 distancia,	 utilizando	 el	\n" + 
						"algoritmo	 de	 Prim,	 aplicado	 al	 componente	 conectado	 (subgrafo)	 más	 grande	 de	 la	\n" + 
						"malla	vial	de	Bogotá.");
				System.out.println(modelo.darMSTPrimHaversine());
				break;
				
			case 9: 
				System.out.println("Encontrar camino menor distancia Haversine según Uber");
				System.out.println("Ingrese latitud de la zona de origen");
				int latorigen2 = lector.nextInt();
				System.out.println("Ingrese longitud de la zona de origen");
				int longiorigen2 = lector.nextInt();
				System.out.println("Ingrese latitud de la zona de destino");
				int latidest2 = lector.nextInt();
				System.out.println("Ingrese longitud de la zona de destino");
				int longidest2 = lector.nextInt();
				System.out.println(modelo.darCostoMinimoHaversine(longiorigen2, latorigen2, longidest2, latidest2));
				break;
				
			case 10:
				System.out.println("A	 partir	 de	 las	 coordenadas	 de	 una	 localización	 geográfica	 de	 la	 ciudad	 (lat,	 lon)	 de	\n" + 
						"origen,	indique	cuáles	vértices	son	alcanzables	para	un	tiempo	T	(en	segundos)	dado	por	\n" + 
						"el	usuario.");
				System.out.println("Ingrese latitud de la zona");
				double lat0 = lector.nextDouble();
				System.out.println("Ingrese longitud de la zona");
				double long0 = lector.nextDouble();
				System.out.println("Ingrese tiempo");
				double time0 = lector.nextDouble();
				Iterable<Vertice> it = modelo.verticesAlcanzables(long0, lat0, time0);
				for(Vertice v : it)
					System.out.println("Id del Vertice: "+ v.getId()+" ("+v.getLatitud()+","+v.getLongitud()+")");
				break;
				
			case 11:
				System.out.println("Calcular	 un	 árbol	 de	 expansión	 mínima	 (MST)	 con	 criterio	 distancia,	 utilizando	 el	\n" + 
						"algoritmo	de	Kruskal,	aplicado	al	componente	conectado	(subgrafo)	más	grande	de	la	\n" + 
						"malla	vial	de	Bogotá.	");
				System.out.println(modelo.darMSTKruskalHaversine());
				break;
			case 12:
				System.out.println("Construir grafo semplificado (Req 10)");
				Graph<VerticeZona> grf = modelo.crearGrafoZonas();
				Iterable<WeighedEdge> lista = grf.edges();
					System.out.println("La cantidad de vertices es: "+grf.V() + " y la de arcos "+ grf.E());
			case 15: 
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;	

			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
