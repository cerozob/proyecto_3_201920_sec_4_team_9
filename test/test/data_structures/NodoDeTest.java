package test.data_structures;

public class NodoDeTest {
	
	private int id;
	private String atri1;
	private String atri2;
	
	public NodoDeTest(int idn, String a1, String a2)
	{
		id = idn;
		atri1 = a1;
		atri2 = a2;
	}
	
	public int darId()
	{
		return id;
	}
	
	public String darA1()
	{
		return atri1;
	}
	
	public String darA2()
	{
		return atri2;
	}
}
