package test.data_structures;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.GrafoNoDirigido;
import model.logic.Haversine;
import model.logic.Vertice;

public class TestGrafoNoDirigido {
	
	public final static String rutaVertices = "";
	public final static String rutaArcos = "";
	
	public final static int tam = 0;

	private GrafoNoDirigido<Integer,Vertice> graf;
	
	@Before
	public void setup() //testCargar
	{
		graf = new GrafoNoDirigido(tam);
		cargar();
	}
	
	@Test
	public void addEdge()
	{
		Haversine haver = new Haversine();
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaArcos)));
			String actual;
			br.skip(1);
			while((actual = br.readLine())!= null)
			{
				String[] vertice = actual.split(" ");
				int idV = Integer.parseInt(vertice[0]);
				Vertice v = graf.getVertex(idV);
				for (int i = 1; i < vertice.length; i++)
				{
					int idOtro = Integer.parseInt(vertice[i]);
					Vertice otro = graf.getVertex(idOtro);
					double costo = haver.distance(v.getLatitud(), v.getLongitud(), otro.getLatitud(), otro.getLongitud())
					graf.addEdge(idV, idOtro, costo);
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void cargar()
	{
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaVertices)));
			String actual;
			br.skip(1);
			while((actual = br.readLine())!= null)
			{
				String[] linea = actual.split(";");
				int id = Integer.parseInt(linea[0]);
				Vertice v = new Vertice(id,
						Double.parseDouble(linea[1]),
						Double.parseDouble(linea[2]),Integer.parseInt(linea[3]));
				graf.addVertex(id,v);
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
